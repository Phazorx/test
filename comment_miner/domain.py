from collections import defaultdict
from datetime import datetime


class Comment(object):
    fbtime_format = '%Y-%m-%dT%H:%M:%S+0000'

    def __init__(self, fb_id, created_at):
        self.fb_id = fb_id
        self.created_at = self.to_datetime(created_at)

    @classmethod
    def to_datetime(cls, date_string):
        return datetime.strptime(date_string, cls.fbtime_format)

    @classmethod
    def to_fbtime(cls, dt):
        return dt.strftime(cls.fbtime_format)

    def __eq__(self, other):
        return self.fb_id == other.fb_id

    def __ne__(self, other):
        return self.__eq__(other)

    def __hash__(self):
        return hash(self.fb_id)


def process_results(results):
    comments = list()
    for result in results:
        for item in result:
            comments.append(Comment(item['id'], item['created_time']))
    return comments


def filter_invalid_comments(comments):
    return set(comments)


def aggregate_comments(comments):
    comments_by_date = defaultdict(int)
    for comment in comments:
        comments_by_date[comment.created_at.strftime('%Y-%m-%d')] += 1
    return comments_by_date
