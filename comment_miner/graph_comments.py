#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
Produces HTML document with a graphs of comments frequency over time for specified facebook post
"""

import sys
import logging
import argparse
from requests_futures.sessions import FuturesSession

from .domain import process_results, filter_invalid_comments, aggregate_comments
from .rendering import render_data_to_document
from .dataminer import get_total_count, prepare_requests, request_manager


def main():
    parser = argparse.ArgumentParser(description=__doc__)
    parser.add_argument('token', type=str, help='FB API Access Token')
    parser.add_argument('entity', nargs='?', type=str, help='FB resource id', default='10151775534413086')
    parser.add_argument('file', nargs='?', type=str, help='result file', default='out.html')
    parser.add_argument('concurrency', nargs='?', type=int, help='result file', default=10)
    parser.add_argument('--debug', default=False, help='output debug information', action='store_true')
    args = parser.parse_args()

    log_level = logging.DEBUG if args.debug else logging.INFO
    logging.basicConfig(level=log_level, stream=sys.stdout, format='%(levelname).1s:%(asctime)s: - %(message)s')
    session = FuturesSession(max_workers=args.concurrency)
    node = args.entity + '/comments'

    total_comments = get_total_count(session, args.token, node)
    prepared_requests = prepare_requests(session, args.token, node, total_comments)
    results = request_manager(prepared_requests)
    comments = process_results(results)
    filtered_comments = filter_invalid_comments(comments)
    logging.info("Expected: {}  Results contain: {}  Unique entries: {}".format(
        total_comments,
        len(comments),
        len(filtered_comments))
    )
    aggregated_comments = aggregate_comments(filtered_comments)
    render_data_to_document(aggregated_comments, args.file)
    logging.info('Report saved as: {}'.format(args.file))

if __name__ == "__main__":
    main()
