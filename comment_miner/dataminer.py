import logging
from functools import partial
from time import sleep
from base64 import b64encode

from requests import codes, exceptions
from concurrent.futures import ThreadPoolExecutor, as_completed

from .config import REQUEST_ATTEMPTS, REQUEST_TIMEOUT, RESULTS_PER_REQUEST


def process_fb_response(session, response):
    if response.status_code == codes.ok:
        response.data = response.json()['data']
    else:
        raise exceptions.HTTPError('got HTTP Code {} from {}'.format(response.status_code, response.text))


def generate_fb_request(session, access_token, node, fbapi='https://graph.facebook.com/v2.2/', **kwargs):
    kwargs['access_token'] = access_token
    return partial(
        session.get,
        fbapi + node,
        params=kwargs,
        timeout=REQUEST_TIMEOUT,
        background_callback=process_fb_response
    )


class CountableTask(object):
    def __init__(self, func):
        self.func = func
        self.counter = 0

    def do(self):
        self.counter += 1
        return self.func()


def prepare_requests(session, token, node, total):
    tasks = set()
    for offset in range(0, total, RESULTS_PER_REQUEST):
        payload = dict(fields='created_time', after=b64encode(str(offset).encode('ascii')), limit=RESULTS_PER_REQUEST)
        tasks.add(CountableTask(generate_fb_request(session, token, node, **payload)))
    return tasks


def request_manager(prepared_requests):
    tasks = dict()
    for request in prepared_requests:
        request_future = request.do()
        tasks[request_future] = request

    monitor = ThreadPoolExecutor(max_workers=1)
    monitor.submit(status_reporter, tasks, 1)

    results = list()
    while any(f.running() for f in tasks):
        for request_future in as_completed(tasks.keys()):
            try:
                response = request_future.result()
                results.append(response.data)
            except (KeyError, ValueError, exceptions.RequestException) as e:
                logging.exception(e)
                task = tasks[request_future]
                if task.counter < REQUEST_ATTEMPTS:
                    retry = task.do()
                    tasks[retry] = task
                    logging.debug("retrying: {}".format(task))
    monitor.shutdown(wait=False)
    return results


def status_reporter(tasks, period):
    while True:
        running = sum(r.running() for r in tasks)
        done = sum(r.done() for r in tasks)
        pending = len(tasks) - running - done
        logging.info("pending: {}  running: {}  done: {}".format(pending, running, done))
        if pending == 0:
            break
        sleep(period)


def get_total_count(session, token, node):
    payload = dict(summary=1, offset=0, limit=1)
    prepared_request = generate_fb_request(session, token, node, **payload)
    response = prepared_request().result()
    if response.status_code != codes.ok:
        raise Exception('HTTP Error from facebook:\n%s', response.text)
    post_data = response.json()
    return post_data['summary']['total_count']
