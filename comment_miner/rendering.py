from jinja2 import Environment, PackageLoader


def render_data_to_document(collated_data, file_name):
    env = Environment(loader=PackageLoader(__package__))
    template = env.get_template('vis.jinja2')

    result = template.render(data=collated_data, period='day')

    with open(file_name, 'w') as fh:
        fh.write(result)
