RESULTS_PER_REQUEST = 500  # limit  for number of elements in single Graph API request
REQUEST_TIMEOUT = 10  # in seconds
REQUEST_ATTEMPTS = 2  # number of attempts per requests to retry failed ones
