# README #

Test exercize for Kampiki/EzyInsights

### Deploy & Run ###

In repository snapshot:
```
$python setup.py sdist
```

Create virtual env and then install the package:
```
(virtualenv)$ pip install {path}/dist/comment_miner-0.1.tar.gz
(virtualenv)$ bin/comment_miner -h
```

Launch CLI script:
```
(virtualenv)$ bin/comment_miner -h
```

### Run ###

In repository snashot:
```
$ python setup.py build
```

Launch CLI script:
```
$ python -m comment_miner.graph_comments -h
```