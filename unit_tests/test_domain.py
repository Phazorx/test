import random
import unittest
from datetime import datetime
from comment_miner.domain import Comment, filter_invalid_comments


class CommentTest(unittest.TestCase):

    def setUp(self):
        self.now = datetime.now().replace(microsecond=0)

    def test_create_comment(self):
        fbtime = self.now.strftime('%Y-%m-%dT%H:%M:%S+0000')
        fb_id = '1'*32

        result = Comment(fb_id, fbtime)
        self.assertEqual(result.fb_id, fb_id)
        self.assertEqual(result.created_at, self.now)

    def test_comment_equality(self):
        fbtime = self.now.strftime('%Y-%m-%dT%H:%M:%S+0000')
        fb_id = '1'*32

        foo = Comment(fb_id, fbtime)
        bar = Comment(fb_id, fbtime)

        self.assertEqual(foo, bar)

    def test_comment_membership(self):
        fbtime = self.now.strftime('%Y-%m-%dT%H:%M:%S+0000')

        foo = Comment('1'*32, fbtime)
        bar = Comment('2'*32, fbtime)
        quux = Comment('3'*32, fbtime)
        comments = {foo, bar}

        self.assertIn(foo, comments)
        self.assertNotIn(quux, comments)


class FilteringTest(unittest.TestCase):
    def test_filter_invalid_duplicates(self):
        some_fbtime = Comment.to_fbtime(datetime.now().replace(microsecond=0))
        comments = [Comment(str(i)*32, some_fbtime) for i in range(1, 11)]
        dupe = random.choice(comments)

        result = filter_invalid_comments(comments + [dupe])

        self.assertEqual(len(set(i.fb_id for i in result)), len(comments))
