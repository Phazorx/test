#!/usr/bin/env python
from setuptools import setup
import os
import codecs

here = os.path.abspath(os.path.dirname(__file__))


def load_requirements(name):
    content = read_local_file(name)
    lines = filter(None, content.splitlines())
    return [l for l in lines if not l.startswith('#')]


def read_local_file(*name_parts):
    file_name = os.path.join(here, *name_parts)
    with codecs.open(file_name, 'r', 'utf-8') as f:
        return f.read()


setup(
    name='comment_miner',
    version='0.1',
    description='Test exercise',
    author='Pavel Kolla',
    author_email='pavelkolla@gmx.net',
    url='https://bitbucket.org/Phazorx/test/',
    install_requires=load_requirements('requirements.txt'),
    tests_require=load_requirements('requirements.txt'),
    packages=['comment_miner'],
    include_package_data=True,
    entry_points={'console_scripts': ['comment_miner = comment_miner.graph_comments:main']},
)
